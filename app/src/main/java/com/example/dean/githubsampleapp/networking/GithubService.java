package com.example.dean.githubsampleapp.networking;

import com.example.dean.githubsampleapp.model.Repos;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface GithubService {

    @GET("repositories?")
    Observable<Repos> getRepos(@Query("q") String name);
}
