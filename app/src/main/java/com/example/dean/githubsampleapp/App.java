package com.example.dean.githubsampleapp;

import android.app.Application;

import com.example.dean.githubsampleapp.components.AppComponent;
import com.example.dean.githubsampleapp.components.DaggerAppComponent;
import com.example.dean.githubsampleapp.networking.NetworkModule;

public class App extends Application {

    private static AppComponent component;

    public static AppComponent getAppComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = buildComponent();
    }

    protected AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .networkModule(new NetworkModule())
                .build();
    }
}
