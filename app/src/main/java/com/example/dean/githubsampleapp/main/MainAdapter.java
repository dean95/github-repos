package com.example.dean.githubsampleapp.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dean.githubsampleapp.R;
import com.example.dean.githubsampleapp.model.Repos;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.RepoViewHolder> {

    private final OnItemClickListener mClickListener;
    private Repos mRepos;
    private Context mContext;

    public MainAdapter(Context context, Repos repos, OnItemClickListener listener) {
        mContext = context;
        mRepos = repos;
        mClickListener = listener;
    }

    @Override
    public RepoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_list_item, null);

        return new RepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepoViewHolder holder, int position) {
        holder.repoNameTextView.setText(mRepos.getItems().get(position).getName());
        holder.authorNameTextView.setText(mRepos.getItems().get(position).getOwner().getLogin());
        holder.watchersTextView.setText(mRepos.getItems().get(position).getWatchers().toString());
        holder.forksTextView.setText(mRepos.getItems().get(position).getForks().toString());
        holder.issuesTextView.setText(mRepos.getItems().get(position).getOpenIssuesCount().toString());

        String imageUrl = mRepos.getItems().get(position).getOwner().getAvatarUrl();

        Picasso.with(mContext)
                .load(imageUrl)
                .into(holder.avatarImageView);
    }

    @Override
    public int getItemCount() {
        return mRepos.getItems().size();
    }

    public class RepoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.iv_avatar)
        ImageView avatarImageView;

        @BindView(R.id.tv_repo_name)
        TextView repoNameTextView;

        @BindView(R.id.tv_author_name)
        TextView authorNameTextView;

        @BindView(R.id.tv_watchers)
        TextView watchersTextView;

        @BindView(R.id.tv_forks)
        TextView forksTextView;

        @BindView(R.id.tv_issues)
        TextView issuesTextView;

        public RepoViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int clickedPosition = getAdapterPosition();
            mClickListener.onItemClick(clickedPosition);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int clickedItemIndex);
    }
}
