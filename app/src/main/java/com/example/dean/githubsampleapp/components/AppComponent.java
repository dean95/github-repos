package com.example.dean.githubsampleapp.components;

import com.example.dean.githubsampleapp.main.MainActivity;
import com.example.dean.githubsampleapp.networking.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {NetworkModule.class})
@Singleton
public interface AppComponent {
    void inject(MainActivity mainActivity);
}
