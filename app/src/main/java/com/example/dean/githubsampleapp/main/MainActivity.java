package com.example.dean.githubsampleapp.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dean.githubsampleapp.App;
import com.example.dean.githubsampleapp.R;
import com.example.dean.githubsampleapp.model.Repos;
import com.example.dean.githubsampleapp.networking.Service;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
        implements IMainView, MainAdapter.OnItemClickListener {

    @BindView(R.id.rv_repositories)
    RecyclerView reposList;

    @BindView(R.id.et_query)
    EditText searchQuery;

    @Inject
    Service service;

    MainPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        App.getAppComponent().inject(this);

        ButterKnife.bind(this);

        init();

        mPresenter = new MainPresenter(this, service);
    }

    @Override
    public void fecthReposSuccess(Repos repos) {
        MainAdapter adapter = new MainAdapter(this, repos, this);
        reposList.setAdapter(adapter);
    }

    @Override
    public void fetchReposFail() {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(int clickedItemIndex) {
        Toast.makeText(this, "Clicked position: " + clickedItemIndex, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_search)
    public void onSearchClicked(View view) {
        mPresenter.getRepos(searchQuery.getText().toString().trim());
    }

    private void init() {
        reposList.setLayoutManager(new LinearLayoutManager(this));
    }
}
