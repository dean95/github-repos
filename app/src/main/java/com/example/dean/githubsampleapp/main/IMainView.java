package com.example.dean.githubsampleapp.main;

import com.example.dean.githubsampleapp.model.Repos;

public interface IMainView {
    void fecthReposSuccess(Repos repos);
    void fetchReposFail();
}
