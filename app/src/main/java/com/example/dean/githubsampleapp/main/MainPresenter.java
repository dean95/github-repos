package com.example.dean.githubsampleapp.main;

import android.util.Log;

import com.example.dean.githubsampleapp.model.Repos;
import com.example.dean.githubsampleapp.networking.Service;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class MainPresenter {
    private final Service service;
    private final IMainView mainView;

    private CompositeSubscription subscriptions;

    public MainPresenter(IMainView view, Service service) {
        this.service = service;
        this.mainView = view;
        subscriptions = new CompositeSubscription();
    }

    public void getRepos(String name) {
        Subscription subscription = service.getRepos(new Service.GetReposCallback() {
            @Override
            public void onSuccess(Repos repos) {
                mainView.fecthReposSuccess(repos);
            }

            @Override
            public void onError() {
                mainView.fetchReposFail();
            }
        },
        name);

        subscriptions.add(subscription);
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }
}
