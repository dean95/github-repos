package com.example.dean.githubsampleapp.networking;

import android.util.Log;

import com.example.dean.githubsampleapp.model.Repos;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class Service {

    private final GithubService githubService;

    public Service(GithubService service) {
        githubService = service;
    }

    public Subscription getRepos(final GetReposCallback callback, String name) {
        return githubService.getRepos(name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Repos>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError();
                    }

                    @Override
                    public void onNext(Repos repos) {
                        callback.onSuccess(repos);
                    }
                });
    }

    public interface GetReposCallback {
        void onSuccess(Repos repos);
        void onError();
    }
}
